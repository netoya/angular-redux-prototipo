import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import createSagaMiddleware from 'redux-saga';

import { NgReduxModule, NgRedux } from '@angular-redux/store';

import { AppComponent } from './app.component';

import { IAppState, createStore } from './redux-saga/redux-saga';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, NgReduxModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>) {
    ngRedux.provideStore(createStore());
  }
}
